package login

import (
	"log"
	"net/http"
	"text/template"

	"github.com/jsign/IG_HA/front/server"
)

const (
	msgEmptyLogin      = "email and password are required"
	msgFailedLogin     = "email and password are not registered, try again"
	msgUnexpectedError = "unexpected"
)

var (
	tmplLogin = template.Must(template.ParseFiles("login/template/login.html"))
)

type loginData struct {
	Failed bool
	Msg    string
}

// RegisterRoutes register routes
func RegisterRoutes(server *server.Server) {
	server.Mux.HandleFunc("/login", getHandler(server)).Methods("GET")
	server.Mux.HandleFunc("/login", postEmailHandler(server)).Methods("POST")
	server.Mux.HandleFunc("/login/google", postGoogleHandler(server)).Methods("POST")
	server.Mux.HandleFunc("/logout", getLogoutHandler(server)).Methods("GET")
}

func postEmailHandler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		formEmail := r.FormValue("email")
		formPassword := r.FormValue("password")
		if formEmail == "" || formPassword == "" {
			tmplLogin.Execute(w, &loginData{Failed: true})
			return
		}
		var user *server.User
		var success bool
		success, user, err = s.API.LoginEmail(formEmail, formPassword)

		if err != nil || !success {
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			se.AddFlash("Invalid email/password, try again")
			err := se.Save(r, w)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		se.Values["user"] = user
		err = se.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "profile", http.StatusFound)
	}
}

func postGoogleHandler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		tokenID := r.FormValue("idtoken")
		if tokenID == "" {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		success, user, err := s.API.LoginGoogle(tokenID)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if !success {
			se.AddFlash("Google account isn't registered, please signup.")
			err := se.Save(r, w)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		se.Values["user"] = user
		err = se.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		log.Println("google login success")
	}
}

func getHandler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user, ok := se.Values["user"].(server.User)
		if ok && user.Email != "" {
			http.Redirect(w, r, "/profile", http.StatusFound)
			return
		}

		msgs := se.Flashes()
		data := &loginData{}
		if len(msgs) > 0 {
			strMsg, ok := msgs[0].(string)
			if ok {
				data.Failed = true
				data.Msg = strMsg
			}
			err = se.Save(r, w)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		tmplLogin.Execute(w, data)
	}
}

func getLogoutHandler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		se.Values["user"] = server.User{}

		err = se.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/login", http.StatusFound)
	}
}
