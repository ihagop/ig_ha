package signup

import (
	"net/http"
	"text/template"

	"github.com/jsign/IG_HA/front/server"
)

var (
	tpl = template.Must(template.ParseGlob("signup/template/*.html"))
)

// RegisterRoutes register routes
func RegisterRoutes(server *server.Server) {
	server.Mux.HandleFunc("/signup", getSignup(server)).Methods("GET")
	server.Mux.HandleFunc("/signup", postSignup(server)).Methods("POST")
	server.Mux.HandleFunc("/signup/google", postSignupGoogle(server)).Methods("POST")
}

func getSignup(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user, ok := se.Values["user"].(server.User)
		if ok && user.Email != "" {
			http.Redirect(w, r, "/profile", http.StatusFound)
			return
		}

		tpl.ExecuteTemplate(w, "signup.html", nil)
	}
}

func postSignup(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user, ok := se.Values["user"].(server.User)
		if ok && user.Email != "" {
			http.Redirect(w, r, "/profile", http.StatusFound)
			return
		}

		email, password := r.FormValue("email"), r.FormValue("password")
		if email == "" || password == "" {
			se.AddFlash("Both fields are mandatory")
			tpl.ExecuteTemplate(w, "signup.html", nil)
			return
		}

		err = s.API.SignupEmailPassword(email, password)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		se.Values["user"] = server.User{
			Email: email,
		}

		se.Save(r, w)
		http.Redirect(w, r, "/profile?edit=true", http.StatusFound)
	}
}
func postSignupGoogle(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		user, ok := se.Values["user"].(server.User)
		if ok && user.Email != "" {
			http.Redirect(w, r, "/profile", http.StatusFound)
			return
		}

		tokenID := r.FormValue("idtoken")
		if tokenID == "" {
			http.Redirect(w, r, "/signup", http.StatusFound)
			return
		}

		email, err := s.API.SignupGoogle(tokenID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		se.Values["user"] = server.User{
			Email: email,
		}
		se.Save(r, w)
		http.Redirect(w, r, "/profile?edit=true", http.StatusFound)
	}
}
