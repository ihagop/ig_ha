package server

import (
	"encoding/gob"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
)

//Backend provides services for the server
type Backend interface {
	SignupEmailPassword(email, password string) error
	SignupGoogle(tokenID string) (email string, err error)
	LoginEmail(email, password string) (success bool, user *User, err error)
	LoginGoogle(tokenID string) (success bool, user *User, err error)
	UpdateProfile(email, fullname, telephone, address string) error
	RecoverAccount(recoveryToken, newPassword string) error
	GenerateRecoveryLink(email string) (cantRecover bool, GUID string, err error)
}

//Server represents the http server
type Server struct {
	Mux   *mux.Router
	API   Backend
	Store *sessions.CookieStore
	http.Server
}

//User represents a user in the system
type User struct {
	Fullname  string
	Address   string
	Email     string
	Telephone string
}

func init() {
	gob.Register(User{})
}

//New creates a new server
func New(listenAddr string, api Backend) *Server {
	router := mux.NewRouter()
	return &Server{
		Mux: router,
		API: api,
		//Store: sessions.NewCookieStore(securecookie.GenerateRandomKey(32)),
		Store: sessions.NewCookieStore([]byte(strconv.Itoa(123456))),
		Server: http.Server{
			Handler:      router,
			Addr:         listenAddr,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
	}
}
