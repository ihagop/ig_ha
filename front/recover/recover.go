package recover

import (
	"net/http"
	"text/template"

	"github.com/jsign/IG_HA/front/server"
)

type recoverModel struct {
	EmailLink   string
	CantRecover bool
}

var tpl = template.Must(template.ParseGlob("recover/template/*.html"))

// RegisterRoutes register routes
func RegisterRoutes(server *server.Server) {
	server.Mux.HandleFunc("/recover", getRecoverHandler(server)).Methods("GET")
	server.Mux.HandleFunc("/recover", postRecoverHandler(server)).Methods("POST")
	server.Mux.HandleFunc("/recover/step2", getRecoverStep2Handler(server)).Methods("GET")
	server.Mux.HandleFunc("/recover/step2", postRecoverStep2Handler(server)).Methods("POST")
}

func getRecoverHandler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user, ok := se.Values["user"].(server.User)
		if ok && user.Email != "" {
			http.Redirect(w, r, "/profile", http.StatusFound)
			return
		}

		tpl.ExecuteTemplate(w, "recover.html", nil)
	}
}

func postRecoverHandler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user, ok := se.Values["user"].(server.User)
		if ok && user.Email != "" {
			http.Redirect(w, r, "/profile", http.StatusFound)
			return
		}

		email := r.FormValue("email")
		if email == "" {
			http.Error(w, "Email can't be empty", http.StatusInternalServerError)
			return
		}

		cantRecover, GUID, err := s.API.GenerateRecoveryLink(email)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		data := &recoverModel{
			EmailLink:   r.URL.String() + "/step2?guid=" + GUID,
			CantRecover: cantRecover,
		}

		tpl.ExecuteTemplate(w, "recoverEmailLink.html", data)
	}
}

func getRecoverStep2Handler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user, ok := se.Values["user"].(server.User)
		if ok && user.Email != "" {
			http.Redirect(w, r, "/profile", http.StatusFound)
			return
		}

		tpl.ExecuteTemplate(w, "recoverStep2.html", nil)
	}
}

func postRecoverStep2Handler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user, ok := se.Values["user"].(server.User)
		if ok && user.Email != "" {
			http.Redirect(w, r, "/profile", http.StatusFound)
			return
		}

		recoveryToken := r.URL.Query().Get("guid")
		if recoveryToken == "" {
			http.Error(w, "Recovery token can't be empty", http.StatusInternalServerError)
			return
		}
		newPassword := r.FormValue("password")
		if newPassword == "" {
			http.Error(w, "New password can't be empty", http.StatusInternalServerError)
			return
		}
		err = s.API.RecoverAccount(recoveryToken, newPassword)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/login", http.StatusFound)
	}
}
