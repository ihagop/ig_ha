package backend

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/jsign/IG_HA/front/server"
)

var errorFatal = errors.New("Fatal error")

//Backend provides services to the frontend
type Backend struct {
	BaseURL    *url.URL
	httpClient *http.Client
}

type signupEmailPasswordReq struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type signupGoogleReq struct {
	TokenID string `json:"tokenID,omitempty"`
}

type signupGoogleRes struct {
	Email string `json:"email,omitempty"`
}

type loginGoogleReq struct {
	TokenID string `json:"tokenID,omitempty"`
}

type loginGoogleRes struct {
	Address   string `json:"address,omitempty"`
	FullName  string `json:"fullName,omitempty"`
	Telephone string `json:"telephone,omitempty"`
	Email     string `json:"email,omitempty"`
	Success   bool   `json:"success,omitempty"`
}

type updateProfileReq struct {
	Email     string `json:"email,omitempty"`
	Fullname  string `json:"fullName,omitempty"`
	Telephone string `json:"telephone,omitempty"`
	Address   string `json:"address,omitempty"`
}

type loginEmailReq struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type loginEmailRes struct {
	Address   string `json:"address,omitempty"`
	FullName  string `json:"fullName,omitempty"`
	Telephone string `json:"telephone,omitempty"`
	Success   bool   `json:"success,omitempty"`
}

type generateRecoveryLinkReq struct {
	Email string `json:"email,omitempty"`
}

type generateRecoveryLinkRes struct {
	CantRecover bool   `json:"cantRecover,omitempty"`
	GUID        string `json:"guid,omitempty"`
}

type recoverAccountReq struct {
	GUID        string `json:"guid,omitempty"`
	NewPassword string `json:"newPassword,omitempty"`
}

// New returns a new api instance
func New(endpoint string) (*Backend, error) {
	baseURL, err := url.Parse(endpoint)

	if err != nil {
		return nil, err
	}

	backend := &Backend{
		BaseURL: baseURL,
		httpClient: &http.Client{
			Timeout: 10 * time.Second,
		},
	}

	return backend, nil
}

//SignupEmailPassword signups a user with email/password
func (b *Backend) SignupEmailPassword(email, password string) error {
	body := signupEmailPasswordReq{
		Email:    email,
		Password: password,
	}
	req, err := b.newReq("POST", "/signup/email", body)
	if err != nil {
		log.Println(err)
		return errorFatal
	}
	res, err := b.exec(req, nil)
	if err != nil {
		log.Println(err)
		return errorFatal
	}
	if res.StatusCode != http.StatusOK {
		log.Println(err)
		return errorFatal
	}
	return nil
}

//SignupGoogle signups a user with Google account
func (b *Backend) SignupGoogle(tokenID string) (email string, err error) {
	body := signupGoogleReq{
		TokenID: tokenID,
	}
	req, err := b.newReq("POST", "/signup/google", body)
	if err != nil {
		log.Println(err)
		return "", errorFatal
	}
	payload := signupGoogleRes{}
	res, err := b.exec(req, &payload)
	if err != nil {
		log.Println(err)
		return "", errorFatal
	}
	if res.StatusCode != http.StatusOK {
		return "", errorFatal
	}
	email = payload.Email

	return
}

//LoginEmail logins a email and password
func (b *Backend) LoginEmail(email, password string) (success bool, user *server.User, err error) {
	body := loginEmailReq{
		Email:    email,
		Password: password,
	}
	req, err := b.newReq("POST", "/login/email", body)
	if err != nil {
		log.Println(err)
		return false, nil, errorFatal
	}
	payload := loginEmailRes{}
	res, err := b.exec(req, &payload)
	if err != nil {
		return false, nil, errorFatal
	}
	if res.StatusCode != http.StatusOK {
		log.Println(err)
		return false, nil, errorFatal
	}

	user = &server.User{
		Address:   payload.Address,
		Fullname:  payload.FullName,
		Telephone: payload.Telephone,
		Email:     email,
	}
	success = payload.Success

	return
}

//LoginGoogle logins a user with a Google account
func (b *Backend) LoginGoogle(tokenID string) (success bool, user *server.User, err error) {
	body := loginGoogleReq{
		TokenID: tokenID,
	}
	req, err := b.newReq("POST", "/login/google", body)
	if err != nil {
		log.Println(err)
		return false, nil, errorFatal
	}
	payload := loginGoogleRes{}
	res, err := b.exec(req, &payload)
	if err != nil {
		log.Println(err)
		return false, nil, errorFatal
	}
	if res.StatusCode != http.StatusOK {
		log.Println(err)
		return false, nil, errorFatal
	}

	user = &server.User{
		Address:   payload.Address,
		Fullname:  payload.FullName,
		Telephone: payload.Telephone,
		Email:     payload.Email,
	}
	success = payload.Success
	return
}

//UpdateProfile updates a user profile
func (b *Backend) UpdateProfile(email, fullname, telephone, address string) error {
	body := updateProfileReq{
		Email:     email,
		Fullname:  fullname,
		Telephone: telephone,
		Address:   address,
	}
	req, err := b.newReq("POST", "/updateprofile", body)
	if err != nil {
		log.Println(err)
		return errorFatal
	}
	res, err := b.exec(req, nil)
	if err != nil {
		log.Println(err)
		return errorFatal
	}
	if res.StatusCode != http.StatusOK {
		log.Println(err)
		return errorFatal
	}
	return nil
}

//RecoverAccount recovers an account using a one-shot recovery token
func (b *Backend) RecoverAccount(guid, newPassword string) error {
	body := recoverAccountReq{
		GUID:        guid,
		NewPassword: newPassword,
	}
	req, err := b.newReq("POST", "/recovery/recover", body)
	if err != nil {
		log.Println(err)
		return errorFatal
	}
	res, err := b.exec(req, nil)
	if err != nil {
		log.Println(err)
		return errorFatal
	}
	if res.StatusCode != http.StatusOK {
		log.Println(err)
		return errorFatal
	}
	return nil
}

//GenerateRecoveryLink generates a recovery link to recover an account
func (b *Backend) GenerateRecoveryLink(email string) (cantRecover bool, GUID string, err error) {
	body := generateRecoveryLinkReq{
		Email: email,
	}
	req, err := b.newReq("POST", "/recovery/generate", body)
	if err != nil {
		log.Println(err)
		return false, "", errorFatal
	}
	payload := generateRecoveryLinkRes{}
	res, err := b.exec(req, &payload)
	if err != nil {
		log.Println(err)
		return false, "", errorFatal
	}
	if res.StatusCode != http.StatusOK {
		log.Println(err)
		return false, "", errorFatal
	}
	cantRecover = payload.CantRecover
	GUID = payload.GUID
	return
}

func (b *Backend) newReq(method, path string, body interface{}) (*http.Request, error) {
	rel := &url.URL{Path: path}
	u := b.BaseURL.ResolveReference(rel)
	var buf []byte
	var err error
	if body != nil {
		buf, err = json.Marshal(body)
		if err != nil {
			return nil, err
		}
	}
	req, err := http.NewRequest(method, u.String(), bytes.NewReader(buf))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "application/json")
	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	return req, nil
}
func (b *Backend) exec(req *http.Request, v interface{}) (*http.Response, error) {
	r, err := b.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(buf, v)
	return r, err
}
