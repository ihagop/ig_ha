package profile

import (
	"net/http"
	"text/template"

	"github.com/jsign/IG_HA/front/server"
)

var (
	tpl = template.Must(template.ParseGlob("profile/template/*.html"))
)

type profile struct {
	User     server.User
	EditMode bool
}

// RegisterRoutes register routes
func RegisterRoutes(server *server.Server) {
	server.Mux.HandleFunc("/profile", getHandler(server)).Methods("GET")
	server.Mux.HandleFunc("/profile", postHandler(server)).Methods("POST")
}

// getHandler serves GET to /profile
func getHandler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		editMode := r.URL.Query().Get("edit") != ""
		valUser := se.Values["user"]
		user, ok := valUser.(server.User)
		if !ok || user.Email == "" {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		w.Header().Set("Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0")
		w.Header().Set("Pragma", "no-cache")
		w.Header().Set("Expires", "0")
		err = tpl.ExecuteTemplate(w, "profile.html", &profile{User: user, EditMode: editMode})
	}
}

// postHandler serves GET to /profile
func postHandler(s *server.Server) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		se, err := s.Store.Get(r, "login")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if se.IsNew {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		valUser := se.Values["user"]
		user, ok := valUser.(server.User)
		if !ok {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		err = s.API.UpdateProfile(user.Email, r.FormValue("fullname"), r.FormValue("telephone"), r.FormValue("address"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		user.Address = r.FormValue("address")
		user.Telephone = r.FormValue("telephone")
		user.Fullname = r.FormValue("fullname")
		se.Values["user"] = user
		err = se.Save(r, w)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/profile", http.StatusFound)
	}
}
