package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/jsign/IG_HA/front/backend"
	"github.com/jsign/IG_HA/front/login"
	"github.com/jsign/IG_HA/front/profile"
	"github.com/jsign/IG_HA/front/recover"
	"github.com/jsign/IG_HA/front/server"
	"github.com/jsign/IG_HA/front/signup"
)

var (
	port        = flag.Int("port", 8081, "port on which to serve")
	backendAddr = flag.String("backendAddr", "http://localhost:8082", "address of the backend")
	s           *server.Server
)

func main() {
	flag.Parse()

	addr := fmt.Sprintf(":%v", *port)
	b, err := backend.New(*backendAddr)

	if err != nil {
		fmt.Printf("error while instantiation the backend: %v", err)
		os.Exit(-1)
	}

	s = server.New(addr, b)
	registerRoutes()

	log.Printf("Listening on %v...\n", addr)
	if err := s.ListenAndServe(); err != nil {
		fmt.Printf("%v", err)
	}
}

func registerRoutes() {
	s.Mux.HandleFunc("/", baseHandler)
	login.RegisterRoutes(s)
	profile.RegisterRoutes(s)
	signup.RegisterRoutes(s)
	recover.RegisterRoutes(s)
}

func baseHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/login", http.StatusFound)
}
