package server

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/jsign/IG_HA/backend/api"
)

//Server is an api server
type Server struct {
	api *api.API
	http.Server
}

type signupEmailPasswordReq struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type signupGoogleReq struct {
	TokenID string `json:"tokenID,omitempty"`
}

type signupGoogleRes struct {
	Email string `json:"email,omitempty"`
}

type loginGoogleReq struct {
	TokenID string `json:"tokenID,omitempty"`
}

type loginGoogleRes struct {
	Address   string `json:"address,omitempty"`
	FullName  string `json:"fullName,omitempty"`
	Telephone string `json:"telephone,omitempty"`
	Email     string `json:"email,omitempty"`
	Success   bool   `json:"success,omitempty"`
}

type updateProfileReq struct {
	Email     string `json:"email,omitempty"`
	Fullname  string `json:"fullName,omitempty"`
	Telephone string `json:"telephone,omitempty"`
	Address   string `json:"address,omitempty"`
}

type loginEmailReq struct {
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type loginEmailRes struct {
	Address   string `json:"address,omitempty"`
	FullName  string `json:"fullName,omitempty"`
	Telephone string `json:"telephone,omitempty"`
	Success   bool   `json:"success,omitempty"`
}

type generateRecoveryLinkReq struct {
	Email string `json:"email,omitempty"`
}

type generateRecoveryLinkRes struct {
	GUID        string `json:"guid,omitempty"`
	CantRecover bool   `json:"cantRecover,omitempty"`
}

type recoverAccountReq struct {
	GUID        string `json:"guid,omitempty"`
	NewPassword string `json:"newPassword,omitempty"`
}

//New returns a new server
func New(addr string, api *api.API) *Server {
	mux := mux.NewRouter()

	server := &Server{
		api: api,
		Server: http.Server{
			Handler:      mux,
			Addr:         addr,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
	}

	mux.HandleFunc("/login/email", server.postLoginEmailHandler).Methods("POST")
	mux.HandleFunc("/login/google", server.postLoginGoogleHandler).Methods("POST")
	mux.HandleFunc("/signup/email", server.postSignupEmailHandler).Methods("POST")
	mux.HandleFunc("/signup/google", server.postSignupGoogleHandler).Methods("POST")
	mux.HandleFunc("/updateprofile", server.postUpdateProfileHandler).Methods("POST")
	mux.HandleFunc("/recovery/generate", server.postRecoveryGenerateHandler).Methods("POST")
	mux.HandleFunc("/recovery/recover", server.postRecoveryRecoverHandler).Methods("POST")

	return server
}

func (s *Server) postUpdateProfileHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var req updateProfileReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = s.api.UpdateProfile(req.Email, req.Address, req.Fullname, req.Telephone)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (s *Server) postSignupGoogleHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var req signupGoogleReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	email, err := s.api.SignupGoogle(req.TokenID)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	res := signupGoogleRes{
		Email: email,
	}
	jsonRes, err := json.Marshal(&res)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonRes)
}

func (s *Server) postSignupEmailHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var req signupEmailPasswordReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = s.api.SignupEmail(req.Email, req.Password)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (s *Server) postLoginGoogleHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var req loginGoogleReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	success, user, err := s.api.LoginGoogle(req.TokenID)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	res := loginGoogleRes{
		Success: success,
	}
	if user != nil {
		res.Email = user.Email
		res.Address = user.Address
		res.FullName = user.FullName
		res.Telephone = user.Telephone
	}
	jsonRes, err := json.Marshal(&res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonRes)
}

func (s *Server) postLoginEmailHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var req loginEmailReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	success, user, err := s.api.LoginEmail(req.Email, req.Password)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	res := loginEmailRes{
		Success: success,
	}
	if user != nil {
		res.Address = user.Address
		res.FullName = user.FullName
		res.Telephone = user.Telephone
	}
	jsonRes, err := json.Marshal(&res)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonRes)
}

func (s *Server) postRecoveryGenerateHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var req generateRecoveryLinkReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	cantRecover, guid, err := s.api.RecoveryGenerate(req.Email)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	res := generateRecoveryLinkRes{
		GUID:        guid,
		CantRecover: cantRecover,
	}
	jsonRes, err := json.Marshal(&res)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonRes)
}

func (s *Server) postRecoveryRecoverHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var req recoverAccountReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = s.api.RecoverAccount(req.GUID, req.NewPassword)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
