package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"

	"github.com/jsign/IG_HA/backend/api"
	"github.com/jsign/IG_HA/backend/server"

	_ "github.com/go-sql-driver/mysql"
)

var (
	listenAddr         = flag.String("port", ":8082", "addr on which to serve")
	dbConnectionString = flag.String("db", "", "mysql connection string")
	googleAPIClientID  = flag.String("clientid", "", "google api client id")
)

func main() {
	flag.Parse()

	database, err := sql.Open("mysql", *dbConnectionString)
	if err != nil {
		panic(err)
	}
	api := api.New(database, *googleAPIClientID)
	server := server.New(*listenAddr, api)

	log.Printf("Backend server listening on %v...\n", *listenAddr)
	if err := server.ListenAndServe(); err != nil {
		fmt.Printf("%v", err)
	}
}
