package api

import (
	"context"
	"crypto/sha256"
	"database/sql"
	"errors"
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"google.golang.org/api/oauth2/v1"
)

//API is the implementation
type API struct {
	db                *sql.DB
	googleAPIClientID string
}

//User represents a user in the system
type User struct {
	Email     string
	Address   string
	FullName  string
	Telephone string
}

//New creates a new API
func New(db *sql.DB, googleAPIClientID string) *API {
	return &API{
		db:                db,
		googleAPIClientID: googleAPIClientID,
	}
}

//LoginEmail logs in an email/password
func (a *API) LoginEmail(email, password string) (success bool, user *User, err error) {
	hash := hashPassword(password)

	var address, fullName, telephone string
	err = a.db.QueryRow("select address, fullName, telephone from users where email=? and passwordHash=? limit 1", email, hash).Scan(&address, &fullName, &telephone)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil, nil
		}
		return false, nil, err
	}

	user = &User{
		Email:     email,
		Address:   address,
		FullName:  fullName,
		Telephone: telephone,
	}
	success = true
	return
}

//SignupGoogle signup a user with Google account logs in an email/password
func (a *API) SignupGoogle(tokenID string) (email string, err error) {
	ti, err := getGoogleTokenInfo(tokenID, a.googleAPIClientID)
	if err != nil {
		return "", err
	}
	_, err = a.db.Exec("insert into users (email, passwordHash, fullName, telephone, address) values (?, '', '', '', '')", ti.Email)

	if err != nil {
		return "", err
	}

	return ti.Email, nil
}

//SignupEmail signup a user with email/password
func (a *API) SignupEmail(email, password string) error {
	_, err := a.db.Exec("insert into users (email, passwordHash, fullName, telephone, address) values (?, ?, '', '', '')", email, hashPassword(password))

	return err
}

//LoginGoogle login a user with Google account
func (a *API) LoginGoogle(tokenID string) (success bool, user *User, err error) {
	ti, err := getGoogleTokenInfo(tokenID, a.googleAPIClientID)
	if err != nil {
		return false, nil, err
	}

	var address, fullName, telephone string
	err = a.db.QueryRow("select address, fullName, telephone from users where email=? limit 1", ti.Email).Scan(&address, &fullName, &telephone)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil, nil
		}
		return false, nil, err
	}

	user = &User{
		Email:     ti.Email,
		Address:   address,
		FullName:  fullName,
		Telephone: telephone,
	}
	success = true
	return
}

//UpdateProfile updates the profile information of a user
func (a *API) UpdateProfile(email, address, fullName, telephone string) error {
	_, err := a.db.Exec("update users set address=?, fullName=?, telephone=? where email=?", address, fullName, telephone, email)

	return err
}

//RecoveryGenerate an recovery link to recover an account
func (a *API) RecoveryGenerate(email string) (cantRecover bool, guid string, err error) {
	guid = uuid.New().String()

	var dummy int
	err = a.db.QueryRow("select 1 from users where email=? and char_length(passwordHash)>0 limit 1", email).Scan(&dummy)
	if err != nil {
		if err == sql.ErrNoRows {
			return true, "", nil
		}
		return
	}

	_, err = a.db.Exec("insert into users_recovery (email, guid) values (?, ?)", email, guid)
	if err != nil {
		return false, "", err
	}

	return
}

//RecoverAccount recovers a new account with a recovery link
func (a *API) RecoverAccount(guid, newPassword string) error {
	tx, err := a.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	var email string
	err = tx.QueryRow("select email from users_recovery where guid=?", guid).Scan(&email)
	if err != nil {
		if err == sql.ErrNoRows {
			return errors.New("the recovery code doesn't exist")
		}
		return err
	}

	_, err = tx.Exec("delete from users_recovery where guid=?", guid)
	if err != nil {
		return err
	}

	_, err = tx.Exec("update users set passwordHash=? where email=?", hashPassword(newPassword), email)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func hashPassword(password string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(password)))
}

func getGoogleTokenInfo(token, clientID string) (*oauth2.Tokeninfo, error) {
	ctx := context.Background()
	svc, err := oauth2.New(http.DefaultClient)
	ti, err := svc.Tokeninfo().IdToken(token).Context(ctx).Do()
	if err != nil {
		return nil, err
	}

	if ti.Audience != clientID {
		return nil, errors.New("token info doesn't match the application")
	}

	return ti, nil
}
